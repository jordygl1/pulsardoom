from setuptools import setup, find_packages

version = 1.2

with open("README.md") as readme_file:
    readme = readme_file.read()

classifiers = [
    "License :: OSI Approved :: GPL License",
    "Natural Language :: English",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.10"
]

requirements = [
    "oschmod",
    "numpy",
    "pygame==2.1.2"
]

packages = [
    "pulsardoom",
    "pulsardoom/bin",
    "pulsardoom/wad"
]

package_data = {
    "":           ["LICENSE",
                   "README.rst"],
    "pulsardoom": ["pulsardoom/bin/pulsardoom",
                   "pulsardoom/wad/doom1.wad"]
}

entry_points = {
    'console_scripts': [
        'pulsardoom=pulsardoom.pulsardoom:_main'
    ]
}

setup(
    name                          = "pulsardoom",
    author                        = "Jordy Gloudemans",
    author_email                  = "jordygl1@gmail.com",
    license                       = "GPL",
    version                       = version,
    url                           = "https://gitlab.com/jordygl1/pulsardoom",
    download_url                  = "https://gitlab.com/jordygl1/pulsardoom/-/archive/v{0}/qblox_instruments-v{0}.zip".format(version),
    description                   = "Doom for Qblox instruments.",
    long_description              = readme,
    long_description_content_type = "text/markdown",
    keywords                      = ["Qblox", "instrument", "Pulsar", "Doom"],
    classifiers                   = classifiers,
    python_requires               = ">=3.10",
    install_requires              = requirements,
    include_package_data          = True,
    packages                      = find_packages(include=packages),
    package_data                  = package_data,
    entry_points                  = entry_points,
    zip_safe                      = False
)