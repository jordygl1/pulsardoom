#ifndef EVENTS_H
#define EVENTS_H

//Includes
#include "doomtype.h"

//Defines
#define MAX_EVENT_TYPE_WIDTH 16
#define MAX_EVENT_KEY_WIDTH  16



//Function prototypes
void event_init(void);

#ifdef __cplusplus
extern "C" {
#endif

void event_connect(void);
void event_close(void);
boolean get_input_event(char *event_type, char* event_key);
void send_backend_event(char *event_type, char *event_str);

#ifdef __cplusplus
}
#endif



#endif
