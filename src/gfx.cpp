//Source includes
#include "const.h"
#include "server.h"
#include "gfx.h"



//Static variable
server *gfx_server;

//Initialize grafics server
void gfx_init(void) {
	gfx_server = new server("gfx_server", GFX_PORT);
	gfx_connect();
}

//Connect grafics server
void gfx_connect(void) {
	while (!gfx_server->connected()) {
		gfx_server->connect();
	}
}

//Close grafics server
void gfx_close(void) {
	gfx_server->disconnect();
}

//Perform a frame update
void gfx_frame_update(void *frame, int frame_size) {
	gfx_connect();
	gfx_server->send_obj("frame", frame, frame_size);
}
