//System includes
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

//Source includes
#include "config.h"
#include "events.h"
#include "sfx.h"
#include "gfx.h"
#include "main.h"



//Functions
extern "C" void D_DoomMain (void);

int main(void) {
	printf("Starting %s...\n", PACKAGE_NAME);

	event_init();
	sfx_init();
	gfx_init();

	D_DoomMain();

	return 0;
}
