#ifndef SFX_H
#define SFX_H

//System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

//Source includes
#include "i_sound.h"


//Function prototypes
void sfx_init(void);

#ifdef __cplusplus
extern "C" {
#endif

void sfx_connect(void);
void sfx_close(void);

boolean sound_module_init(boolean use_sfx_prefix);
void sound_module_shutdown(void);
int sound_module_get_sfx_lump_num(sfxinfo_t *sfxinfo);
void sound_module_update(void);
void sound_module_update_sound_params(int channel, int vol, int sep);
int sound_module_start_sound(sfxinfo_t *sfxinfo, int channel, int vol, int sep);
void sound_module_stop_sound(int channel);
boolean sound_module_sound_is_playing(int channel);
void sound_module_cache_sounds(sfxinfo_t *sounds, int num_sounds);

boolean music_module_init(void);
void music_module_shutdown(void);
void music_module_set_volume(int volume);
void music_module_pause_music(void);
void music_module_resume_music(void);
void *music_module_register_song(void *data, int len);
void music_module_unregister_song(void *handle);
void music_module_play_song(void *handle, boolean looping);
void music_module_stop_song(void);
boolean music_module_is_playing(void);
void music_module_poll(void);

#ifdef __cplusplus
}
#endif



#endif
