//System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

//Source includes
#include "const.h"
#include "server.h"
#include "main.h"
#include "sfx.h"
#include "i_sound.h"
#include "w_wad.h"
#include "z_zone.h"
#include "memio.h"
#include "mus2mid.h"



//Static variables
snddevice_t snd_musicdevice = SNDDEVICE_WAVEBLASTER;
snddevice_t snd_sfxdevice   = SNDDEVICE_WAVEBLASTER;

server *sfx_sounds_server;
server *sfx_music_server;

typedef struct music_reg_struct {
    char *data;
    int  len;
    bool in_use;
} t_music_reg;
t_music_reg music_reg[256];

typedef struct music_state_struct {
    int  handle;
    bool looping;
    int  volume;
    bool playing;
} t_music_state;
t_music_state music_state = {
    0,
    false,
    0,
    false
};



//-----------------------------------------------------------------------------
//Server functions
//-----------------------------------------------------------------------------

//Initialize sound effects and music servers
void sfx_init(void) {
    sfx_sounds_server = new server("sfx_sounds_server", SFX_SOUND_PORT);
    sfx_music_server = new server("sfx_music_server", SFX_MUSIC_PORT);
    sfx_connect();
}

//Connect sound effects and music servers
void sfx_connect(void) {
    while (!sfx_sounds_server->connected()) {
        sfx_sounds_server->connect();
    }

    while (!sfx_music_server->connected()) {
        sfx_music_server->connect();

        if (music_state.playing) {
            music_module_set_volume(music_state.volume);
            music_module_play_song((void *) music_state.handle, music_state.looping);
        }
    }
}

//Close sound effects and music servers
void sfx_close(void) {
    sfx_sounds_server->disconnect();
    sfx_music_server->disconnect();
}



//-----------------------------------------------------------------------------
//Sound functions
//-----------------------------------------------------------------------------
boolean sound_module_init(boolean use_sfx_prefix) {
    sfx_connect();

    return true;
}

void sound_module_stop_sound(int channel) {
    sfx_connect();
    sfx_sounds_server->send_obj("channel", &channel, sizeof(int));
    sfx_sounds_server->send_msg("stop");
}

void sound_module_shutdown(void) {
    sound_module_stop_sound(-1);
}

int sound_module_get_sfx_lump_num(sfxinfo_t *sfxinfo) {
    char sfxlump_name[20];

    //Walk link
    while (sfxinfo->link) sfxinfo = sfxinfo->link;

    //Create lump name
    sprintf(sfxlump_name, "DS%s", (const char *) sfxinfo->name);

    //Check if the sound lump exists
    if (W_CheckNumForName(sfxlump_name) == -1) return 0;

    //Return lump num
    return W_GetNumForName(sfxlump_name);
}

void sound_module_update(void) {
    return;
}

void sound_module_update_sound_params(int channel, int vol, int sep) {
    sfx_connect();
    sfx_sounds_server->send_obj("channel", &channel, sizeof(int));
    sfx_sounds_server->send_obj("volume", &vol, sizeof(int));
    //Not doing stereo audio for now (see https://github.com/libretro/libretro-prboom/blob/master/libretro/libretro_sound.c)
}

int sound_module_start_sound(sfxinfo_t *sfxinfo, int channel, int vol, int sep) {
    char sfxlump_name[20];
    int  sfxlump_num;
    int  sfxlump_len;
    char *data;
    int  data_len;

    //Handle channel and volume
    sound_module_update_sound_params(channel, vol, sep);

    //Walk link
    while (sfxinfo->link) sfxinfo = sfxinfo->link;

    //Create lump name
    sprintf(sfxlump_name, "DS%s", (const char *) sfxinfo->name);

    //Check if the sound lump exists
    if (W_CheckNumForName(sfxlump_name) == -1) return 0;

    //Get lump num and lump length
    sfxlump_num = W_GetNumForName(sfxlump_name);
    sfxlump_len = W_LumpLength(sfxlump_num);

    //Check format (8 byte header + at least 1 sample)
    if (sfxlump_len < 9) return 0;

    //Get lump
    data = (char *) W_CacheLumpNum(sfxlump_num, PU_STATIC);

    //Get sound from lump
    data += 4;
    data_len = *((int *) data);
    data_len -= 32;
    data += 20;

    //Send sound
    sfx_connect();
    sfx_sounds_server->send_obj("sound", data, data_len);

    //Release lump
    W_ReleaseLumpNum(sfxlump_num);

    return channel;
}

boolean sound_module_sound_is_playing(int channel) {
    return true;
}

void sound_module_cache_sounds(sfxinfo_t *sounds, int num_sounds) {
    return;
}



//-----------------------------------------------------------------------------
//Music functions
//-----------------------------------------------------------------------------

boolean music_module_init(void) {
    for (int i=0; i<ELEM_CNT(music_reg); i++) {
        music_reg[i].in_use = false;
    }
    sfx_connect();

    return true;
}

void music_module_stop_song(void) {
    sfx_connect();
    sfx_music_server->send_msg("stop");
    music_state.playing = false;
}

void music_module_shutdown(void){
    music_module_stop_song();
}

void music_module_set_volume(int volume) {
    sfx_connect();
    sfx_music_server->send_obj("volume", &volume, sizeof(int));
    music_state.volume = volume;
}

void music_module_pause_music(void) {
    sfx_connect();
    sfx_music_server->send_msg("pause");
    music_state.playing = false;
}

void music_module_resume_music(void) {
    sfx_connect();
    sfx_music_server->send_msg("resume");
    music_state.playing = true;
}

void *music_module_register_song(void *data, int len) {
    bool reg_full;
    int  i;

    reg_full = true;
    for (i=0; i<ELEM_CNT(music_reg); i++) {
        if (!music_reg[i].in_use) {
            music_reg[i].data   = (char *) data;
            music_reg[i].len    = len;
            music_reg[i].in_use = true;
            reg_full            = false;
            break;
        }
    }

    if (reg_full) {
        printf("ERROR: Music register overflow.\n");
        return ((void *) 0);
    } else {
        return ((void *) i);
    }
}

void music_module_unregister_song(void *handle) {
    music_reg[(int) handle].in_use = false;
}

void music_module_play_song(void *handle, boolean looping) {
    MEMFILE *mus;
    MEMFILE *mid;
    char    *data;
    int     data_len;
    bool    result;
    char    *musptr;
    int     muslen;

    sfx_connect();

    if (music_reg[(int) handle].in_use) {
        data     = music_reg[(int) handle].data;
        data_len = music_reg[(int) handle].len;
        musptr  = data;
        muslen   = data_len;
    } else {
        return;
    }

    mus = mem_fopen_read(data, data_len);
    mid = mem_fopen_write();
    result = mus2mid(mus, mid);

    if (result) {
        while (musptr < data + data_len - sizeof(musheader)) {
            if (!strncmp((const char*) musptr, "MUS\x1a", 4)) {
                mem_fclose(mus);
                mus    = mem_fopen_read(musptr, muslen);
                result = mus2mid(mus, mid);
                break;
            }
            musptr++;
            muslen--;
        }
    }

    if (result == 0) {
        mem_get_buf(mid, (void **) &data, (size_t *) &data_len);

        sfx_connect();
        sfx_music_server->send_obj("looping", &looping, sizeof(boolean));
        sfx_music_server->send_obj("song", data, data_len);
    }

    mem_fclose(mus);
    mem_fclose(mid);
    music_state.handle  = (int) handle;
    music_state.looping = looping;
    music_state.playing = true;
}

boolean music_module_is_playing(void) {
    return false;
}

void music_module_poll(void) {
    return;
}



//-----------------------------------------------------------------------------
//Music modules
//-----------------------------------------------------------------------------

//Create sound and music modules
sound_module_t sfx_sound_module = {
    &snd_sfxdevice,
    1,
    &sound_module_init,
    &sound_module_shutdown,
    &sound_module_get_sfx_lump_num,
    &sound_module_update,
    &sound_module_update_sound_params,
    &sound_module_start_sound,
    &sound_module_stop_sound,
    &sound_module_sound_is_playing,
    &sound_module_cache_sounds,
};

music_module_t sfx_music_module = {
    &snd_musicdevice,
    1,
    &music_module_init,
    &music_module_shutdown,
    &music_module_set_volume,
    &music_module_pause_music,
    &music_module_resume_music,
    &music_module_register_song,
    &music_module_unregister_song,
    &music_module_play_song,
    &music_module_stop_song,
    &music_module_is_playing,
    &music_module_poll
};