#ifndef GFX_H
#define GFX_H



//Function prototypes
void gfx_init(void);

#ifdef __cplusplus
extern "C" {
#endif

void gfx_connect(void);
void gfx_close(void);
void gfx_frame_update(void *frame, int frame_size);

#ifdef __cplusplus
}
#endif



#endif
