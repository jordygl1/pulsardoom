//Source includes
#include "const.h"
#include "server.h"
#include "events.h"



//Input server
server *event_server;

//Initialize event server
void event_init(void) {
	event_server = new server("event_server", EVENT_PORT);
	event_connect();
}

//Wait for connection with event client
void event_connect(void) {
	while (!event_server->connected()) {
		event_server->connect();
	}
}

//Close event connection
void event_close(void) {
	event_server->disconnect();
}

//Get input event strings
boolean get_input_event(char *event_type, char* event_key) {
    boolean new_event;

    event_connect();
    new_event = event_server->ready_to_read();
    if (new_event) {
        //Set event type chars
        event_server->read_obj(event_type, (void *) event_key);
    }

    return new_event;
}

//Send backend event string and object
void send_backend_event(char *event_type, char *event_str) {
    int event_str_size = strlen(event_str) + 1;
    
    event_connect();
    event_server->send_obj(event_type, (void *) event_str, event_str_size);
}
