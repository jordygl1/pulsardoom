//System includes
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/tcp.h>

//Source includes
#include "server.h"



//Constructor
server::server(const char *server_name, int server_port) {
    int opt = 1;

    //Set status
    connected_state = false;

    //Set name
    name = server_name;

    //Creating socket file descriptor
    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd == 0) {
        printf("%s: Failed to create socket file descriptor.\n", name);
        exit(-1);
    }

    //Forcefully attaching socket to the port
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        printf("%s: Failed to attach socket to port.\n", name);
        exit(-1);
    }
    if (setsockopt(server_fd, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(opt))) {
        printf("%s: Failed to attach socket to port.\n", name);
        exit(-1);
    }

    socket_addr.sin_family      = AF_INET;
    socket_addr.sin_addr.s_addr = INADDR_ANY;
    socket_addr.sin_port        = htons(server_port);
    if (bind(server_fd, (struct sockaddr *) &socket_addr, sizeof(socket_addr)) < 0) {
        printf("%s: Failed to attach socket to port.\n", name);
        exit(-1);
    }
}

//Destructor
server::~server() {
    //Close socket
    disconnect();
    close(server_fd);
}

//Connect
bool server::connect() {
    int socket_addr_len = sizeof(socket_addr);

    printf("%s: Waiting for connection...\n", name);
    if (listen(server_fd, 1) < 0) {
        printf("%s: Failed to attach socket to port.\n", name);
        return true;
    }

    //Accept connection
    new_socket = accept(server_fd, (struct sockaddr *) &socket_addr, (socklen_t *) &socket_addr_len);
    if (new_socket < 0) {
        printf("%s: Failed to except connection.\n", name);
        return true;
    }

    //Set status
    printf("%s: Connected...\n", name);
    connected_state = true;

    return false;
}

//Return if connected
bool server::connected() {
    return connected_state;
}

//Disconnect
void server::disconnect() {
    //Close socket
    close(new_socket);

    //Set status
    connected_state = false;
}

//Data available to read
bool server::ready_to_read() {
    struct timeval tv;
    fd_set         rfds;

    tv.tv_sec  = 0;
    tv.tv_usec = 0;

    FD_ZERO(&rfds);
    FD_SET(new_socket, &rfds);

    return select(new_socket + 1, &rfds, NULL, NULL, &tv) > 0;
}

//Send object part
void server::send_obj_part(void *obj, int obj_size) {
    int sent_size = 0;
    int buf_size;

    //Send object block by block
    while (sent_size < obj_size) {
        if (obj_size - sent_size > MAX_BLK_SIZE) {
            buf_size = MAX_BLK_SIZE;
        } else {
            buf_size = obj_size - sent_size;
        }
        buf_size = send(new_socket, (char *) obj + sent_size, buf_size, 0);
        sent_size += buf_size;
    }
}

//Read object part
void server::read_obj_part(void *obj, int obj_size) {
    int read_size = 0;
    int buf_size;

    //Read object block by block
    while (read_size < obj_size) {
        if (obj_size - read_size > MAX_BLK_SIZE) {
            buf_size = MAX_BLK_SIZE;
        } else {
            buf_size = obj_size - read_size;
        }
        buf_size = read(new_socket, (char *) obj + read_size, buf_size);
        read_size += buf_size;
    }
}

//Send object to client
void server::send_obj(char *id, void *obj, int obj_size) {
    int id_size = strlen(id) + 1;
    
    //Send ID string size
    send_obj_part((void *) &id_size, sizeof(int));

    //Send ID string
    send_obj_part((void *) id, id_size);

    //Send object size
    send_obj_part((void *) &obj_size, sizeof(int));

    //Send object data
    send_obj_part(obj, obj_size);
}

//Read object from client
int server::read_obj(char *id, void *obj) {
    int id_size  = 0;
    int obj_size = 0;

    //Read ID string size
    read_obj_part((void *) &id_size, sizeof(int));

    //Read ID string
    read_obj_part(id, id_size);

    //Read object size
    read_obj_part((void *) &obj_size, sizeof(int));

    //Read object data
    read_obj_part(obj, obj_size);

    return obj_size;
}
