#pragma once

//System includes
#include <vector>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <string.h>

//Defines
#define MAX_BLK_SIZE 64 * 1024

#define send_msg(msg) send_obj(msg, nullptr, 0)
#define read_msg(msg) read_obj(msg, nullptr)



class server {
    public:
        //Variables
        const char *name;

        //Functions
        server(const char *server_name, int server_port);
        ~server();
        bool connect();
        bool connected();
        void disconnect();

        //Functions
        bool ready_to_read();
        void send_obj(char *id, void *obj, int obj_size);
        int  read_obj(char *id, void *obj);

    private:
        //Variables
        int                server_fd;
        struct sockaddr_in socket_addr;
        int                new_socket;
        bool               connected_state;

        //Functions
        void send_obj_part(void *obj, int obj_size);
        void read_obj_part(void *obj, int obj_size);
};