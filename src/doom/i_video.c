// Emacs style mode select   -*- C++ -*-
//-----------------------------------------------------------------------------
//
// $Id:$
//
// Copyright (C) 1993-1996 by id Software, Inc.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// $Log:$
//
// DESCRIPTION:
//	DOOM graphics stuff for X11, UNIX.
//
//-----------------------------------------------------------------------------

#include "config.h"
#include "v_video.h"
#include "m_argv.h"
#include "d_event.h"
#include "d_main.h"
#include "i_video.h"
#include "z_zone.h"

#include "tables.h"
#include "doomkeys.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "events.h"
#include "sfx.h"
#include "gfx.h"

// The screen buffer; this is modified to draw things to the screen

typedef struct
{
    byte r;
    byte g;
    byte b;
} col_t;

byte *I_VideoBuffer = NULL;
col_t *I_VideoBuffer_tmp = NULL;

// If true, game is running as a screensaver

boolean screensaver_mode = false;

// Flag indicating whether the screen is currently visible:
// when the screen isnt visible, don't render the screen

boolean screenvisible;

// Mouse acceleration
//
// This emulates some of the behavior of DOS mouse drivers by increasing
// the speed when the mouse is moved fast.
//
// The mouse input values are input directly to the game, but when
// the values exceed the value of mouse_threshold, they are multiplied
// by mouse_acceleration to increase the speed.

float mouse_acceleration = 2.0;
int mouse_threshold = 10;

// Gamma correction level to use

int usegamma = 0;

int usemouse = 0;

// If true, keyboard mapping is ignored, like in Vanilla Doom.
// The sensible thing to do is to disable this if you have a non-US
// keyboard.

int vanilla_keyboard_mapping = true;

// Palette

static col_t rgb_palette[256];

void I_InitGraphics (void)
{
	I_VideoBuffer = (byte*)Z_Malloc (SCREENWIDTH * SCREENHEIGHT, PU_STATIC, NULL);
    I_VideoBuffer_tmp = (col_t*)Z_Malloc (sizeof(col_t) * SCREENWIDTH * SCREENHEIGHT, PU_STATIC, NULL);

	screenvisible = true;
}

void I_ShutdownGraphics (void)
{
	Z_Free (I_VideoBuffer);
}

void I_StartFrame (void)
{

}

boolean I_GetEvent (void)
{
    boolean new_event;
    char    type[MAX_EVENT_TYPE_WIDTH];
    char    key[MAX_EVENT_KEY_WIDTH];
    event_t event;

    do {
        new_event = get_input_event(type, key);
        if (new_event) {
            if (strcmp(type, "quit") == 0) {
                printf("Client is closing, so closing game.\n");
                printf("Goodbye.\n");
                return true;
            } else if (strcmp(type, "sleep") == 0) {
                printf("Client is closing, so going to sleep.\n");
                printf("Goodnight.\n");
                send_backend_event("exit", "");
                event_close();
                sfx_close();
                gfx_close();

                //Wait for clients to reconnect
                event_connect();
                sfx_connect();
                gfx_connect();
            } else if (strcmp(type, "down") == 0 || strcmp(type, "up") == 0) {
                event.type = type[0] == 'd' ? ev_keydown : ev_keyup;
                event.data1 = -1;
                event.data2 = -1;
                event.data3 = -1;

                //Action keys
                if (strcmp(key, "up") == 0) {
                    event.data1 = KEY_UPARROW;
                } else if (strcmp(key, "down") == 0) {
                    event.data1 = KEY_DOWNARROW;
                } else if (strcmp(key, "left") == 0) {
                    event.data1 = KEY_LEFTARROW;
                } else if (strcmp(key, "right") == 0) {
                    event.data1 = KEY_RIGHTARROW;
                } else if (strcmp(key, "return") == 0 || strcmp(key, "enter") == 0) {
                    event.data1 = KEY_ENTER;
                } else if (strcmp(key, "left ctrl") == 0 || strcmp(key, "right ctrl") == 0) {
                    event.data1 = KEY_FIRE;
                } else if (strcmp(key, "left shift") == 0 || strcmp(key, "right shift") == 0) {
                    event.data1 = KEY_RSHIFT;
                } else if (strcmp(key, "left alt") == 0 || strcmp(key, "right alt") == 0) {
                    event.data1 = KEY_RALT;
                } else if (strcmp(key, "space") == 0) {
                    event.data1 = KEY_USE;

                //Control keys
                } else if (strcmp(key, "escape") == 0) {
                    event.data1 = KEY_ESCAPE;
                } else if (strcmp(key, "tab") == 0) {
                    event.data1 = KEY_TAB;
                } else if (strcmp(key, "pause") == 0) {
                    event.data1 = KEY_PAUSE;
                } else if (strcmp(key, "capslock") == 0) {
                    event.data1 = KEY_CAPSLOCK;
                } else if (strcmp(key, "backspace") == 0) {
                    event.data1 = KEY_BACKSPACE;
                } else if (strcmp(key, "del") == 0) {
                    event.data1 = KEY_DEL;
                } else if (strcmp(key, "home") == 0) {
                    event.data1 = KEY_HOME;
                } else if (strcmp(key, "end") == 0) {
                    event.data1 = KEY_END;

                //Function keys
                } else if (strcmp(key, "f2") == 0) {
                    event.data1 = KEY_F2;
                } else if (strcmp(key, "f3") == 0) {
                    event.data1 = KEY_F3;
                } else if (strcmp(key, "f4") == 0) {
                    event.data1 = KEY_F4;
                } else if (strcmp(key, "f5") == 0) {
                    event.data1 = KEY_F5;
                } else if (strcmp(key, "f6") == 0) {
                    event.data1 = KEY_F6;
                } else if (strcmp(key, "f7") == 0) {
                    event.data1 = KEY_F7;
                } else if (strcmp(key, "f8") == 0) {
                    event.data1 = KEY_F8;
                } else if (strcmp(key, "f9") == 0) {
                    event.data1 = KEY_F9;
                } else if (strcmp(key, "f10") == 0) {
                    event.data1 = KEY_F10;
                } else if (strcmp(key, "f11") == 0) {
                    event.data1 = KEY_F11;

                //Others (including weapon keys)
                } else {
                    event.data1 = key[0];
                }

                D_PostEvent(&event);
            }
        }
    } while (new_event);

    return false;
}

boolean I_StartTic (void)
{
	return I_GetEvent();
}

void I_UpdateNoBlit (void)
{
}

void I_FinishUpdate (void)
{
    int   x;
    int   y;
    byte  index;

    for (y = 0; y < SCREENHEIGHT; y++)
    {
        for (x = 0; x < SCREENWIDTH; x++)
        {
            index = I_VideoBuffer[y * SCREENWIDTH + x];
            I_VideoBuffer_tmp[y * SCREENWIDTH + x] = rgb_palette[index];
        }
    }

    gfx_frame_update((void *) I_VideoBuffer_tmp, sizeof(col_t) * SCREENHEIGHT * SCREENWIDTH);
}

//
// I_ReadScreen
//
void I_ReadScreen (byte* scr)
{
    memcpy (scr, I_VideoBuffer, SCREENWIDTH * SCREENHEIGHT);
}

//
// I_SetPalette
//
void I_SetPalette (byte* palette)
{
	int i;

	for (i = 0; i < 256; i++)
	{
		rgb_palette[i] = *((col_t*) palette);
		palette += 3;
	}
}

// Given an RGB value, find the closest matching palette index.

int I_GetPaletteIndex (int r, int g, int b)
{
    int best, best_diff, diff;
    int i;

    best = 0;
    best_diff = INT_MAX;

    for (i = 0; i < 256; ++i)
    {
        diff = (r - rgb_palette[i].r) * (r - rgb_palette[i].r)
             + (g - rgb_palette[i].g) * (g - rgb_palette[i].g)
             + (b - rgb_palette[i].b) * (b - rgb_palette[i].b);

        if (diff < best_diff)
        {
            best = i;
            best_diff = diff;
        }

        if (diff == 0)
        {
            break;
        }
    }

    return best;
}

void I_BeginRead (void)
{
}

void I_EndRead (void)
{
}

void I_SetWindowTitle (char *title)
{
}

void I_GraphicsCheckCommandLine (void)
{
}

void I_SetGrabMouseCallback (grabmouse_callback_t func)
{
}

void I_EnableLoadingDisk (void)
{
}

void I_BindVideoVariables (void)
{
}

void I_DisplayFPSDots (boolean dots_on)
{
}

void I_CheckIsScreensaver (void)
{
}
