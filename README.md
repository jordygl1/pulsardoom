# PulsarDoom

Copyright (C) Jordy Gloudemans (2020) - All rights reserved.

## Introduction
In the desire to find out if Qblox instruments were able to run the all time classic Doom from 1993 and thereby answering the long pondered
question "Can it run Doom?", I created a port called PulsarDoom that actually runs pretty well on them. Of course the Qblox instruments do not have a
screen or a keyboard to interact with the game, so this port runs the game code as a backend on the instrument and a very simple frontend on a host PC
for the player to interact with. Essentially, the game is streamed, putting a modern twist on this classic. This Readme explains how to install and run PulsarDoom as well as how to build the backend, so without further ado:

"Rip and tear until it is done." - [Doom 2016](https://youtu.be/NgAm664NloA)

## User guide
To (un)install and run the game in Windows, first make sure to install a compatible terminal like Git Bash or Cygwin. The frontend uses SSH to upload and start the backend on the instrument. To do this, it will try to configure SSH keys to prevent you from having to repeatedly provide passwords. If you experience problems with this, you can also do this manually by executing `ssh-keygen -q -t rsa -P "" -f ~/.ssh/id_rsa`.

Further streaming of the game will use regular socket interfaces. Make sure you have a solid connection with the instrument since bandwidth restrictions can cause lag in the game. If you experience the game as a slideshow, this is the most likely cause.

This build comes with the shareware WAD of Doom 1. Technically, this WAD can be replaced by the full game WAD of Doom 1 or any other compatible game. Games that have been tested:
- Doom (shareware and full)
- Doom II
- Final Doom: Plutonia Experiment
- Final Doom: TNT: Evilution
- Chex Quest

It is possible to add additional WAD files to the WAD directory (pulsardoom/wad) when the Python package is installed in editable mode. The frontend will let you select which game WAD to install on the instrument. Make sure that the WAD file name and extension is written in lower case.

Notes:
- Only runs on Windows. Linux support is currently broken due to an incompatiblity of the Pygame package and the SDL mixer for music and SFX.
- Replace any IP address with the IP address of the instrument in any of the following steps.
- Root access through SSH is required to install and run the game. Ask an admin for the password.
- Does not work with Powershell because of missing tools (e.g. cat, etc). When running in Windows, it is advised to use Git Bash or Cygwin.

### Prerequisites
- [Python 3.10](https://www.python.org/downloads/release/python-3100/)

### Clone repo
~~~sh
git clone https://gitlab.com/jordygl1/pulsardoom.git
cd pulsardoom
~~~

### Install
~~~sh
python -m pip install .
pulsardoom -i 192.168.0.2
~~~

### Run
~~~sh
pulsardoom 192.168.0.2
~~~

### Uninstall
~~~sh
pulsardoom -u 192.168.0.2
python -m pip uninstall pulsardoom
~~~

## Backend
The backend is based on the game code of Doom 1 from 1993, which is mainly coded in C. I have made it cross-compilable for the ARM A9 processors in the Xilinx Zynq-7000 series FPGAs and have added a C++ layer to deal with socket interfaces
and the likes for streaming purposes. The game comes with a prebuild backend, but the backend can also easily be rebuild by calling the Makefile. Make sure that you install the Xilinx SDK in /opt or change the path to the cross-compiler in the Makefile accordingly.

### Prerequisites
- Make
- GCC/G++ for ARM (install through [Xilinx SDK](https://www.xilinx.com/support/download.html))

### Build
~~~sh
make
~~~

## Based on
- [Chocolate Doom](https://github.com/chocolate-doom/chocolate-doom)
- [stm32doom](https://github.com/floppes/stm32doom)