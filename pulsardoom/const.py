#-- include -------------------------------------------------------------------

import os

from dataclasses import dataclass

#-- class ---------------------------------------------------------------------

@dataclass
class const:
    conn = {"user":            "root",
            "default_ip_addr": "192.168.0.2"}

    install = {"bin":        "pulsardoom",
               "bin_dir":    os.path.relpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "bin")),
               "wad_dir":    os.path.relpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "wad")),
               "target_dir": "/home/{}".format(conn["user"])}

    events = {"port": 25001,
              "tic":  0.3}

    sfx = {"music_port":     25002,
           "sound_port":     25003,
           "sample_rate":    22050, #Sounds are at 11025Hz, but this is needed for pygame v2.
           "sample_size":    8,
           "num_init_chan":  1,
           "num_sound_chan": 8,
           "buffer_size":    64}

    gfx = {"port":         25004,
           "default_res":  "1280x800",
           "sourcewidth":  320,
           "sourceheight": 200}
