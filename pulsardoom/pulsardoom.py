#-- include -------------------------------------------------------------------

import sys
import os
import getopt
import glob
import time
import pygame
import subprocess

from pulsardoom import const, ssh, events, sfx, gfx

#------------------------------------------------------------------------------
def _handle_input_args():
    """
    Handle input arguments
    """

    try:
        #Set reference
        opt_ref = "-r res -i -u -h ip_addr".split()

        #Get list of input options and arguments
        opt_str        = ''.join([opt[1] if opt[0] == '-' else ':' for opt in opt_ref[:-1]])
        opt_list, args = getopt.getopt(sys.argv[1:], opt_str)

        #Format dictionary
        in_arg_dict = {}
        for opt in opt_list:
            if opt[0][1] == "h":
                raise

            idx = opt_ref[:-1].index(opt[0])
            if idx < len(opt_ref) - 1 and opt_ref[idx + 1][0] != '-':
                in_arg_dict[opt_ref[idx + 1]] = opt[1]

        in_arg_dict["flags"] = [opt[0][1] for opt in opt_list]

        if len(args):
            in_arg_dict[opt_ref[-1]] = args[0]

        return in_arg_dict
    except:
        _show_manual()
        sys.exit(0)

#------------------------------------------------------------------------------
def _show_manual():
    """
    Show manual
    """

    print("pulsardoom [options] [IP adress]\n")
    print("Options:")
    print("-r resolution : Set window resolution")
    print("-i            : Install on device")
    print("-u            : Uninstall from device")
    print("-h            : Show this message\n")
    print("Arguments:")
    print("IP address : Device IP address; default={}\n".format(const.conn["default_ip_addr"]))

#------------------------------------------------------------------------------
def _is_installed(ip_addr : str):
    """
    Check if PulsarDoom is installed on the device
    """

    #Check for existence of target directory
    ls_proc = ssh.cmd_proc(ip_addr, "ls")
    ls = ls_proc.stdout.read().split("\n")

    return const.install["bin"] in ls

#------------------------------------------------------------------------------
def _install(ip_addr : str):
    """
    Install PulsarDoom on the device
    """

    #Uninstall if already installed
    if _is_installed(ip_addr):
        print("Found previous PulsarDoom installation.")
        _uninstall(ip_addr)

    print("Installing PulsarDoom on device...")

    #Select WAD file to install
    wad_files = [os.path.split(wad_file)[-1] for wad_file in glob.glob(os.path.join(const.install["wad_dir"], '*.wad'))]
    if len(wad_files) > 1:
        print("Found {} game WAD files:".format(len(wad_files)))
        for idx, wad_file in enumerate(wad_files):
            print("[{}]: {}".format(idx, str.lower(os.path.splitext(wad_file)[0])))

        while True:
            try:
                wad_files[0] = wad_files[int(input("Select game WAD to install [0-{}]: ".format(len(wad_files)-1)))]
                break
            except Exception:
                print("Invalid input.")
    elif len(wad_files) < 1:
        raise Exception("Could not find a WAD file to install.")

    #Create target directory
    dir = const.install["target_dir"] + '/' + const.install["bin"]
    ssh.cmd(ip_addr, "mkdir -p " + dir)

    #Copy binary and WAD files
    ssh.scp(ip_addr, os.path.join(const.install["bin_dir"], const.install["bin"]), dir + "/" + const.install["bin"])
    ssh.scp(ip_addr, os.path.join(const.install["wad_dir"], wad_files[0]), dir + "/" + str.lower(wad_files[0]))

    #Make binary executable
    ssh.cmd(ip_addr, "chmod +x " + dir + '/' + const.install["bin"])

#------------------------------------------------------------------------------
def _uninstall(ip_addr : str):
    """
    Uninstall PulsarDoom from the device
    """

    print("Uninstalling Pulsardoom from device...")

    #Delete the binary and WAD files and any save data
    ssh.cmd(ip_addr, "rm -rf {}/{}".format(const.install["target_dir"], const.install["bin"]))
    ssh.cmd(ip_addr, "rm -rf {}/.{}".format(const.install["target_dir"], const.install["bin"]))

#------------------------------------------------------------------------------
def _execute_backend(ip_addr : str):
    """
    Execute PulsarDoom backend on the device
    """

    print("Starting PulsarDoom backend...")

    #Check if the game is installed on the device
    if not _is_installed(ip_addr):
        raise Exception("Pulsardoom is not installed on the instrument. Please run the installation first.")

    #Run backend
    return ssh.cmd_proc(ip_addr, "cd {0}; ./{0}".format(const.install["bin"]))

#------------------------------------------------------------------------------
def _execute_frontend(ip_addr : str, res : str):
    """
    Execute PulsarDoom frontend
    """

    print("Starting PulsarDoom frontend...")

    #Initialize pygame
    pygame.mixer.pre_init(frequency=const.sfx["sample_rate"], size=const.sfx["sample_size"], channels=const.sfx["num_init_chan"], buffer=const.sfx["buffer_size"])
    pygame.init()

    #Initialize event handler
    event_handler = events(ip_addr)
    time.sleep(const.events["tic"]) #Wait for a game tic to pass

    #Initialize SFX mixer
    sfx_mixer = sfx(ip_addr)
    time.sleep(const.events["tic"]) #Wait for a game tic to pass

    #Initialize GFX renderer
    gfx_renderer = gfx(ip_addr, res)

    #Game loop
    while True:
        #Handle event
        res = event_handler.handle()

        #Get and draw frame
        gfx_renderer.render(res)

        #Play sounds and music
        sfx_mixer.play_sounds();
        sfx_mixer.play_music();

#------------------------------------------------------------------------------
def _terminate_backend(ip_addr : str, proc : subprocess.Popen):
    """
    Terminate PulsarDoom backend
    """

    try:
        #Try to gracefully terminate
        ssh.cmd_proc_terminate(proc)

        #The gracefull termination closes the SSH connection, but does not always terminate the
        #process it was running, leaving a zombie process. This not so gracefull solution kills
        #any residual backend processes on the device.
        ssh.cmd_proc(ip_addr, "killall " + const.install["bin"])
    except:
        pass

#------------------------------------------------------------------------------
def _main():
    """
    Main application
    """

    #Set variable defaults
    in_arg_dict  = {"ip_addr"     : const.conn["default_ip_addr"],
                    "res"         : const.gfx["default_res"],
                    "flags"       : [""]}
    exit_code    = 0
    backend_proc = None

    #Get input arguments
    in_arg_dict = {**in_arg_dict, **_handle_input_args()}

    #Print input arguments
    print("--- PulsarDoom ---")
    print("Summary:")
    print("IP address     : {}".format(in_arg_dict["ip_addr"]))
    print("Resolution     : {}".format(in_arg_dict["res"]))
    print("Flags          : {}\n".format(in_arg_dict["flags"]))

    try:
        #Install
        if 'i' in in_arg_dict["flags"]:
            _install(in_arg_dict["ip_addr"])
        #Uninstall
        elif 'u' in in_arg_dict["flags"]:
            _uninstall(in_arg_dict["ip_addr"])
        #Run
        else:
            backend_proc = _execute_backend(in_arg_dict["ip_addr"])
            _execute_frontend(in_arg_dict["ip_addr"], in_arg_dict["res"])
    except KeyboardInterrupt as e:
        print("Caught keyboard interrupt...")
    except Exception as e:
        print("{}: {}".format(type(e).__name__, str(e)))
        exit_code = 1
    finally:
        _terminate_backend(in_arg_dict["ip_addr"], backend_proc)
        sys.exit(exit_code)

#-- main ----------------------------------------------------------------------
if __name__ == "__main__":
    _main()
