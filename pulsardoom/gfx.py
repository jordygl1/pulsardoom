#-- include -------------------------------------------------------------------

from posixpath import relpath
import numpy
import pygame
import pygame.surfarray

from pulsardoom import const, client

#-- class ---------------------------------------------------------------------

class gfx():

    #--------------------------------------------------------------------------
    def __init__(self, ip_addr : str, res : str = "640x400", frame_cnt_enable : bool = False):
        """
        Constructor
        """

        #Initialize variables
        self._ip_addr          = ip_addr
        self._frame_cnt_enable = frame_cnt_enable
        self._frame_cnt        = 0
        self._frame            = {"new":  False,
                                  "data": None}

        #Create screen and fill it with black
        self._create_screen(res)
        self._screen["object"].fill((255, 255, 255))
        pygame.display.set_caption("PulsarDoom")

        #Open socket
        self._gfx_client = client("gfx_client", self._ip_addr, const.gfx["port"])

    #--------------------------------------------------------------------------
    def _create_screen(self, res : str):
        """
        Create screen dictionary with resolution and object
        """

        #Parse resolution string
        res = res.lower().split('x')
        if len(res) != 2:
            raise Exception("Unsupported resolution format (WxH).")

        try:
            screen = {}
            screen["width"]  = int(res[0])
            screen["height"] = int(res[1])
        except Exception as err:
            raise Exception("Invalid resolution integers.")

        #Create screen object
        screen["object"] = pygame.display.set_mode([screen["width"], screen["height"]], pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE)
        self._screen = screen

    #--------------------------------------------------------------------------
    def _update_frame_cnt(self):
        """
        Update and print frame count
        """

        if self._frame_cnt_enable:
            self._frame_cnt += 1
            print("Frame #: {}". format(self._frame_cnt))

    #--------------------------------------------------------------------------
    def _get_frame_object(self):
        """
        Get frame object
        """

        frame = {"new":  False,
                 "id":   "",
                 "data": None}

        if self._gfx_client.ready_to_read():
            frame["id"], frame["data"] = self._gfx_client.read_obj()
            if frame["id"] == "frame" and len(frame["data"]) == const.gfx["sourcewidth"] * const.gfx["sourceheight"] * 3: # * 3 because RGB
                frame["new"] = True
                self._update_frame_cnt()
            else:
                raise Exception("Invalid GFX frame.")

        return frame

    #--------------------------------------------------------------------------
    def _draw_frame(self, frame : dict):
        """
        Draw frame
        """

        if frame["new"]:
            self._frame = frame

            np_frame = numpy.frombuffer(frame["data"], dtype=numpy.dtype('B'))
            np_frame = numpy.reshape(np_frame, (const.gfx["sourceheight"], const.gfx["sourcewidth"], 3))
            np_frame = numpy.transpose(np_frame, (1, 0, 2))
            surface  = pygame.surfarray.make_surface(np_frame)
            surface  = pygame.transform.scale(surface, (self._screen["width"], self._screen["height"]))

            self._screen["object"].blit(surface, (0, 0))
            pygame.display.flip()

    #--------------------------------------------------------------------------
    def _adjust_res(self, res : str, redraw : bool):
        """
        Adjust resolution
        """

        self._create_screen(res)
        if redraw:
            self._draw_frame(self._frame)

    #--------------------------------------------------------------------------
    def render(self, res : str = None):
        """
        Render frames
        """

        #Get frame object
        frame = self._get_frame_object()

        #Adjust resolution if required
        if res is not None:
            self._adjust_res(res, not frame["new"])

        #Draw frame
        self._draw_frame(frame)
