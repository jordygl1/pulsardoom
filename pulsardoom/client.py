#-- include -------------------------------------------------------------------

import socket
import select
import struct

#-- class ---------------------------------------------------------------------

class client():

    #Static variable
    _size_size = 4
    _blk_size  = 64 * 1024

    #--------------------------------------------------------------------------
    def __init__(self, name : str, ip_addr : str, port : int, timeout = 10.0):
        """
        Constructor
        """

        #Initialize variables
        self._name = name

        #Open socket
        self._socket_ifc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket_ifc.settimeout(timeout)
        self._socket_ifc.connect((ip_addr, port))

    #--------------------------------------------------------------------------
    def close(self):
        """
        Close socket
        """

        self._socket_ifc.close()

    #--------------------------------------------------------------------------
    def ready_to_read(self):
        """
        Ready to read data
        """

        rlist, _, _ = select.select([self._socket_ifc], [], [], 0)
        return (len(rlist) > 0)

    #--------------------------------------------------------------------------
    def _send_obj_part(self, obj : bytearray):
        """
        Send part of object
        """

        sent_size = 0
        while sent_size < len(obj):
            if len(obj) - sent_size > client._blk_size:
                buf_size = client._blk_size
            else:
                buf_size = len(obj) - sent_size
            buf_size += self._socket_ifc.send(obj[sent_size:sent_size + buf_size])
            sent_size += buf_size

    #--------------------------------------------------------------------------
    def _read_obj_part(self, size : int):
        """
        Read part of object
        """

        obj = bytearray()
        while len(obj) < size:
            if size - len(obj) > client._blk_size:
                buf_size = client._blk_size
            else:
                buf_size = size - len(obj)
            obj += self._socket_ifc.recv(buf_size)
        return obj

    #--------------------------------------------------------------------------
    def send_obj(self, id: str, obj : bytearray):
        """
        Send object to server
        """

        #Send ID string size
        id = (id + "\0").encode("ASCII") #Add termination character
        size = struct.pack('I', len(id))
        self._send_obj_part(size)

        #Send ID string
        self._send_obj_part(id)

        #Send object size
        size = struct.pack('I', len(obj))
        self._send_obj_part(size)

        #Send object data
        self._send_obj_part(obj)

    #--------------------------------------------------------------------------
    def read_obj(self):
        """
        Get object from server
        """

        #Read ID string size
        size = self._read_obj_part(client._size_size)
        size = struct.unpack('I', size)[0]

        #Read ID string
        id = self._read_obj_part(size).decode("ASCII")[:-1] #Removing termination character

        #Read object size
        size = self._read_obj_part(client._size_size)
        size = struct.unpack('I', size)[0]

        #Read object data
        obj = self._read_obj_part(size)

        return id, obj

    #--------------------------------------------------------------------------
    def send_msg(self, msg : str):
        """
        Send message to server
        """

        self.send_obj(msg, bytearray())

    #--------------------------------------------------------------------------
    def read_msg(self):
        """
        Get message from server
        """

        id, _ = self.read_obj()

        return id
