#-- include -------------------------------------------------------------------

import os
import oschmod
import stat
import subprocess

from abc        import abstractmethod
from pulsardoom import const

#-- class ---------------------------------------------------------------------

class ssh:

    #------------------------------------------------------------------------------
    @abstractmethod
    def _cmd(cmd: str, verbose : bool = False):
        """
        Execute command. In- and output is not captured and go directly through stdin/stdout.
        """

        if verbose:
            print(cmd)
        os.system(cmd)

    #------------------------------------------------------------------------------
    @abstractmethod
    def _set_ssh_key(ip_addr: str):
        """
        Set an SSH key for the instrument
        """

        #Generate SSH key
        key_dir      = os.path.join(os.path.relpath(os.path.expanduser("~")), ".ssh")
        prvt_keyfile = os.path.join(key_dir, "id_rsa")
        pub_keyfile  = prvt_keyfile + ".pub"
        if not os.path.isfile(pub_keyfile):
            if not os.path.isdir(key_dir):
                os.mkdir(key_dir)
            ssh._cmd('ssh-keygen -q -t rsa -P "" -f {}'.format(prvt_keyfile))
            oschmod.set_mode(prvt_keyfile, stat.S_IRUSR)
            oschmod.set_mode(pub_keyfile, stat.S_IRUSR | stat.S_IWUSR)

        #Copy generated SSH key to the device (using _cmd to make use of stdin/stdout)
        if os.path.isfile(pub_keyfile):
            ssh._cmd('cat {} | ssh {}@{} "cat >> ~/.ssh/authorized_keys"'.format(pub_keyfile, const.conn["user"], ip_addr))
        else:
            raise Exception('Missing public SSH key file ({}). Please generate one using "ssh-keygen -t rsa" before continuing.'.format(os.path.abspath(pub_keyfile)))

    #------------------------------------------------------------------------------
    @abstractmethod
    def cmd(ip_addr: str, cmd: str, verbose : bool = False):
        """
        Execute command over SSH. SSH key is set before executing the command.
        In- and output is not captured and go directly through stdin/stdout.
        """

        ssh._set_ssh_key(ip_addr)
        cmd = 'ssh {}@{} "{}"'.format(const.conn["user"], ip_addr, cmd)
        ssh._cmd(cmd, verbose)

    #------------------------------------------------------------------------------
    @abstractmethod
    def scp(ip_addr: str, source: str, dest: str, verbose : bool = False):
        """
        Execute copy to device over SSH. SSH key is set before executing the command.
        Output goes directly to stdout to show copy progress.
        """

        ssh._set_ssh_key(ip_addr)
        cmd = 'scp {} {}@{}:{}'.format(source, const.conn["user"], ip_addr, dest)
        ssh._cmd(cmd, verbose)



    #------------------------------------------------------------------------------
    @abstractmethod
    def _cmd_proc(cmd: str, verbose : bool = False):
        """
        Execute command and return process object with redirected in- and output.
        """

        if verbose:
            print(cmd)
        proc = subprocess.Popen(cmd, shell=True, text=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if verbose:
            print(proc.stdout.read())
        return proc

    #------------------------------------------------------------------------------
    @abstractmethod
    def cmd_proc(ip_addr: str, cmd: str, verbose : bool = False):
        """
        Execute command over SSH and return process with redirected in- and output.
        SSH key is set before executing the command.
        """

        ssh._set_ssh_key(ip_addr)
        cmd = 'ssh {}@{} "{}"'.format(const.conn["user"], ip_addr, cmd)
        return ssh._cmd_proc(cmd, verbose)

    #------------------------------------------------------------------------------
    @abstractmethod
    def cmd_proc_terminate(proc : subprocess.Popen, timeout : float = 1.0):
        """
        Terminate command running over SSH
        """

        ret = 0

        #Check if process object exists
        if proc is not None:
            #Gracefully terminate if possible
            try:
                proc.terminate()
                ret = proc.wait(timeout)
            except:
                #If process is still running kill it
                try:
                    proc.kill()
                    ret = proc.wait(timeout)
                except:
                    raise Exception("Backend did not properly terminate.")

        return ret
