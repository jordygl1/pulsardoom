#-- include -------------------------------------------------------------------

import time
import struct
import numpy
import pygame

from io         import BytesIO
from pulsardoom import const, client

#-- class ---------------------------------------------------------------------

class sfx():

    #--------------------------------------------------------------------------
    def __init__(self, ip_addr):
        """
        Constructor
        """

        #Initialize variables
        self._ip_addr            = ip_addr
        self._song_looping       = False
        self._sel_sound_channels = []

        #Initialize mixer for sounds
        pygame.mixer.set_num_channels(const.sfx["num_sound_chan"])
        self._channels = [pygame.mixer.Channel(channel) for channel in range(0, const.sfx["num_sound_chan"])]

        #Open socket
        self._sfx_sounds_client = client("sfx_sounds_client", self._ip_addr, const.sfx["sound_port"])
        time.sleep(0.1) #Allow second server to start
        self._sfx_music_client = client("sfx_music_client", self._ip_addr, const.sfx["music_port"])

    #--------------------------------------------------------------------------
    def play_sounds(self):
        """
        Play sounds
        """

        #Get sounds. Because of the high number of sounds being played, we handle
        #this in a while loop here, so we can catch multiple sounds within one game loop.
        while self._sfx_sounds_client.ready_to_read():
            id, data = self._sfx_sounds_client.read_obj()

            if id ==  "channel":
                channel = struct.unpack('i', data)[0]
                if channel < 0:
                    self._sel_sound_channels = self._channels
                elif channel < const.sfx["num_sound_chan"]:
                    self._sel_sound_channels = [self._channels[channel]]
                else:
                    raise Exception("Invalid channel index (channel = {})".format(channel))
            elif id ==  "volume":
                volume = struct.unpack('i', data)[0]
                for channel in self._sel_sound_channels:
                    channel.set_volume(volume)
            elif id == "sound":
                data = numpy.frombuffer(data, dtype=numpy.uint8)
                data = bytearray(data.repeat(4, axis=0)) #Needed for pygame v2. Also see const.py.
                sound = pygame.mixer.Sound(buffer=data)
                for channel in self._sel_sound_channels:
                    channel.play(sound, 0)
            elif id == "stop":
                for channel in self._sel_sound_channels:
                    channel.stop()
            else:
                raise Exception("Invalid SFX sound command ID.")

    #--------------------------------------------------------------------------
    def play_music(self):
        """
        Play music
        """

        #Get songs
        if self._sfx_music_client.ready_to_read():
            id, data = self._sfx_music_client.read_obj()

            if id ==  "volume":
                volume = struct.unpack('i', data)[0]
                pygame.mixer.music.set_volume(volume)
            elif id == "pause":
                pygame.mixer.music.pause()
            elif id == "resume":
                pygame.mixer.music.unpause()
            elif id == "looping":
                self._song_looping = struct.unpack('?', data)[0]
            elif id == "song":
                song = BytesIO(data)
                loops = -1 if self._song_looping else 0
                pygame.mixer.music.load(song)
                pygame.mixer.music.play(loops = loops)
            elif id == "stop":
                pygame.mixer.music.stop()
            else:
                raise Exception("Invalid SFX music command ID.")
