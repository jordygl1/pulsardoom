#-- include -------------------------------------------------------------------

import time
import pygame

from pulsardoom import const, client

#-- class ---------------------------------------------------------------------

class events():

    #--------------------------------------------------------------------------
    def __init__(self, ip_addr : str, sleep_on_exit : bool = True):
        """
        Constructor
        """

        #Initialize variables
        self._ip_addr       = ip_addr
        self._sleep_on_exit = sleep_on_exit

        #Open socket
        self._event_client = client("event_client", self._ip_addr, const.events["port"])

    #--------------------------------------------------------------------------
    def _send_input_event(self, event_type : str, event_key : str = ""):
        """
        Send input event through type and key indicators
        """

        self._event_client.send_obj(event_type, (event_key + "\0").encode("ASCII"))

    #--------------------------------------------------------------------------
    def _send_quit_req(self):
        """
        Send quit request
        """

        if self._sleep_on_exit:
            self._send_input_event("sleep")
        else:
            self._send_input_event("quit")
        time.sleep(const.events["tic"]) #Wait for a game tic to pass
        self._quit()

    #--------------------------------------------------------------------------
    def _quit(self):
        """
        Quit
        """

        pygame.display.quit()
        exit(0)

    #--------------------------------------------------------------------------
    def _send_key(self, event : pygame.event):
        """
        Send key event
        """

        event_type = "down" if event.type == pygame.KEYDOWN else "up"
        self._send_input_event(event_type, pygame.key.name(event.key))

    #--------------------------------------------------------------------------
    def _handle_pygame_events(self):
        """
        Handle pygame events
        """

        #Get pygame events
        events = pygame.event.get()

        #Parse pygame events
        res = None
        for event in events:
            if event.type == pygame.QUIT:
                self._send_quit_req()
            elif event.type == pygame.VIDEORESIZE:
                res = str(event.w) + 'x' + str(event.h)
            elif event.type == pygame.KEYDOWN or event.type == pygame.KEYUP:
                self._send_key(event)

        return res

    #--------------------------------------------------------------------------
    def _handle_backend_events(self):
        """
        Handle backend events
        """

        #Parse backend events
        if self._event_client.ready_to_read():
            id, data = self._event_client.read_obj()

            if id == "exit":
                self._quit()
            elif id == "error":
                raise Exception("Backend terminated with error:\n{}".format(data.decode("ASCII")[:-1]))
            else:
                raise Exception("Invalid backend event command ID.")

    #--------------------------------------------------------------------------
    def handle(self):
        """
        Handle events
        """

        res = self._handle_pygame_events()
        self._handle_backend_events()

        return res