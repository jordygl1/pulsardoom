TARGET                    = pulsardoom
BINDIR                    = $(TARGET)/bin
WADDIR                    = $(TARGET)/wad
SRCDIR                    = src
DOOMDIR                   = doom

SRC_MAIN                  = main.cpp server.cpp events.cpp sfx.cpp gfx.cpp
SRC_DOOM                  = dummy.c am_map.c doomdef.c doomstat.c dstrings.c d_event.c d_items.c d_iwad.c d_loop.c d_main.c d_mode.c d_net.c f_finale.c f_wipe.c g_game.c hu_lib.c hu_stuff.c info.c i_cdmus.c i_endoom.c i_joystick.c i_main.c i_scale.c i_sound.c i_system.c i_timer.c i_video.c memio.c mus2mid.c m_argv.c m_bbox.c m_cheat.c m_config.c m_controls.c m_fixed.c m_menu.c m_misc.c m_random.c p_ceilng.c p_doors.c p_enemy.c p_floor.c p_inter.c p_lights.c p_map.c p_maputl.c p_mobj.c p_plats.c p_pspr.c p_saveg.c p_setup.c p_sight.c p_spec.c p_switch.c p_telept.c p_tick.c p_user.c r_bsp.c r_data.c r_draw.c r_main.c r_plane.c r_segs.c r_sky.c r_things.c sha1.c sounds.c statdump.c st_lib.c st_stuff.c s_sound.c tables.c v_video.c wi_stuff.c w_checksum.c w_file.c w_file_stdc.c w_main.c w_wad.c z_zone.c

C_SRC                     = $(addprefix $(DOOMDIR)/,$(SRC_DOOM))
C_OBJS                    = $(addprefix $(SRCDIR)/, $(C_SRC:.c=.o))
C_OBJ                     = $(subst $(SRCDIR)/,$(BINDIR)/,$(C_OBJS))

CXX_SRC                   = $(SRC_MAIN)
CXX_OBJS                  = $(addprefix $(SRCDIR)/, $(CXX_SRC:.cpp=.o))
CXX_OBJ                   = $(subst $(SRCDIR)/,$(BINDIR)/,$(CXX_OBJS))

CFLAGS                    = -g -c -Wall -Wno-sign-compare -fPIC -O2 -I $(SRCDIR) -I $(SRCDIR)/$(DOOMDIR)
CXXFLAGS                  = -g -c -Wall -Wno-sign-compare -fPIC -std=c++11 -I $(SRCDIR) -I $(SRCDIR)/$(DOOMDIR)
LDFLAGS                   = -g -Wall -fPIC

export PATH               += :/opt/Xilinx/SDK/2018.3/gnu/aarch32/lin/gcc-arm-linux-gnueabi/bin
export ARCH               := arm
export CROSS_COMPILE      := arm-linux-gnueabihf-
export CROSS_COMPILE_ARGS := ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE)

export CC                 := $(CROSS_COMPILE)gcc
export CXX                := $(CROSS_COMPILE)$(CXX)
export AR                 := $(CROSS_COMPILE)$(AR)
export LD                 := $(CROSS_COMPILE)$(LD).bfd

DUMMY                     :=$(shell if ! [ -d $(BINDIR) ]; then mkdir $(BINDIR); fi)
DUMMY                     :=$(shell if ! [ -d $(BINDIR)/$(DOOMDIR) ]; then mkdir $(BINDIR)/$(DOOMDIR); fi)

DEV_USER                  ?=root
DEV_IP                    ?=192.168.0.2
INSTALL_DIR               ?=/home/$(DEV_USER)

$(BINDIR)/$(TARGET): $(C_OBJ) $(CXX_OBJ)
	$(CXX) $(LDFLAGS) -o $@ $(C_OBJ) $(CXX_OBJ)

$(BINDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $< -o $@

$(BINDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $< -o $@

phony: clean install uninstall run

clean:
	@rm -rf $(BINDIR)/*

install:
	@ssh $(DEV_USER)@$(DEV_IP) "mkdir -p $(INSTALL_DIR)/$(TARGET)"
	@scp $(BINDIR)/$(TARGET) $(DEV_USER)@$(DEV_IP):$(INSTALL_DIR)/$(TARGET)
	@scp $(WADDIR)/doom1.wad $(DEV_USER)@$(DEV_IP):$(INSTALL_DIR)/$(TARGET)

uninstall:
	@ssh $(DEV_USER)@$(DEV_IP) "rm -rf $(INSTALL_DIR)/$(TARGET)"
	@ssh $(DEV_USER)@$(DEV_IP) "rm -rf $(INSTALL_DIR)/.$(TARGET)"

run:
	@ssh $(DEV_USER)@$(DEV_IP) "cd $(TARGET);./$(TARGET)"